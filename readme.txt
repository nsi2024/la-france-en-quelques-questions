# Protocole d'utilisation

Pour démarrer le jeux, il faut d'entrer le prénom des deux joueurs:
Il faut enter le premier prénom dans la case appropriée puis cliquer sur "valider"
Ensuite, il faut enlever le premier prénom à l'aide de la souris, entrer le deuxième prénom, cliquer sur "valider" puis sur "afficher les joueurs" et pour finir sur "GO".
Ce dernier clique permet d'afficher le placement des deux joueurs ainsi que les liaisons entre les villes.

Pour commencer à jouer, il faut à tour de rôle lancer le dé en cliquant sur le bouton "lancer le de" le résultat s'affiche alors à côté du bouton ainsi que les villes où le joueur peut aller.
Il faut alors cliquer sur l'une des villes qui s'affichent en rouge. Une question s'affiche, il faut alors choisir entre la réponse 1 et la réponse 2. Si la réponse est correct le score augmente.

Il faut répéter cette action jusqu'à ce que le message indiquant un gagnant s'affiche.

Bon jeu
