########## Importer les modules necessaires ##############
from tkinter import *
from random import *
from random import randint
import time
from tkinter.font import Font
##########################################################
##########    Fonctions ##################################
##########################################################

def clic(event):
    global x_clic, y_clic
    x_clic=event.x
    y_clic=event.y
    deplacement()

def aff():
    '''
    fonction qui retiens le nom des deux joueurs
    '''
    global nom_player_1, nom_player_2, tour_player
    t=Champ.get()
    if tour_player:
        nom_player_1 = t
        tour_player = False
    elif not tour_player:
        nom_player_2 = t
        tour_player = True

def prenom():
    '''
    fonction qui affiche les deux prénoms de la couleur choisie au départ
    '''
    if nom_player_1 != '':
        Canevas.create_text(250,123,text=nom_player_1,fill=couleur_player_1,font=police)
        Lab_1 = Label(Mafenetre, textvariable= str_score_player_1, fg ='black', bg ='white', font = police)
        Lab_1.place(x=250,y=150)
        Canevas.create_oval(x_player_1+10,y_player_1+10,x_player_1-10,y_player_1-10,fill='green')
    if nom_player_2 != '':
        Canevas.create_text(1100,123,text=nom_player_2,fill=couleur_player_2,font=police)
        Lab_2 = Label(Mafenetre, textvariable= str_score_player_2, fg ='black', bg ='white', font = police)
        Lab_2.place(x=1100,y=150)
        Canevas.create_oval(x_player_2+10,y_player_2+10,x_player_2-10,y_player_2-10,fill='orange')

def fin():
    '''
    qui enleve les boutons et emplacements pour ecrire les prenoms
    '''
    Lab.place(x=-85,y=-96)
    Champ.place(x=-43,y=-292)
    bouton.place(x=-43,y=-319)
    bouton2.place(x=-177,y=-319)
    bouton3.place(x=-43,y=-369)
    adjacence()


def de():
    global x_player_1, y_player_1, x_player_2, y_player_2, tour_player
    '''
    qui affiche le de
    '''
    Canevas.create_rectangle(200,900,100,990,fill='light grey')
    n=randint(1,3)
    if n==1:
        Canevas.create_oval(160,935,135,960,fill='black')
    elif n==2:
        Canevas.create_oval(170,960,195,985,fill='black')
        Canevas.create_oval(105,910,130,935,fill='black')
    elif n==3:
        Canevas.create_oval(170,960,195,985,fill='black')
        Canevas.create_oval(105,910,130,935,fill='black')
        Canevas.create_oval(160,935,135,960,fill='black')
    if tour_player:
        affichage_ville(x_player_1, y_player_1, n)
    elif not tour_player :
        affichage_ville(x_player_2, y_player_2, n)

def affichage_ville(x, y, de):
    '''
    qui affiche les villes sur lesquelles ont peut aller en fonction du resultat du de
    '''
    global L_adj, tour_player
    for i in range(len(Villes)):
        x_ville=Villes[i][2]
        y_ville=Villes[i][3]
        Canevas.create_oval(x_ville+10,y_ville+10,x_ville-10,y_ville-10,fill='yellow')
    L_adj = []
    Canevas.create_oval(x_player_1+10,y_player_1+10,x_player_1-10,y_player_1-10,fill='green')
    Canevas.create_oval(x_player_2+10,y_player_2+10,x_player_2-10,y_player_2-10,fill='orange')
    for i in range(len(Villes)):
        if x == Villes[i][2] and y == Villes[i][3] :
            if de == 1:
                for s in Adjacence[i]:
                        L_adj.append(s)
                for k in L_adj :
                    if Villes[k][2] != x and Villes[k][3] != y:
                        Canevas.create_oval(Villes[k][2]+10,Villes[k][3]+10,Villes[k][2]-10,Villes[k][3]-10,fill='red')
            elif de == 2:
                for f in Adjacence[i] :
                    for s in Adjacence[f]:
                        L_adj.append(s)
                for k in L_adj :
                    if Villes[k][2] != x and Villes[k][3] != y:
                        Canevas.create_oval(Villes[k][2]+10,Villes[k][3]+10,Villes[k][2]-10,Villes[k][3]-10,fill='red')
            elif de == 3:
                for f in Adjacence[i] :
                    for s in Adjacence[f]:
                        for m in Adjacence[s]:
                            L_adj.append(m)
                for k in L_adj :
                    if Villes[k][2] != x and Villes[k][3] != y:
                        Canevas.create_oval(Villes[k][2]+10,Villes[k][3]+10,Villes[k][2]-10,Villes[k][3]-10,fill='red')

def deplacement():
    '''
    qui deplace le pion sur la ville sur laquelle ont a clique
    '''
    global x_player_1, y_player_1, region_player_1, x_player_2, y_player_2, region_player_2, L_adj, tour_player
    for i in L_adj:
        if x_clic <= Villes[i][2] + 10 and x_clic >= Villes[i][2] - 10 and y_clic <= Villes[i][3] + 10 and y_clic >= Villes[i][3] - 10 :
            if tour_player :
                x_player_1 = Villes[i][2]
                y_player_1 = Villes[i][3]
                region_player_1 = Villes[i][1]
                tour_player = False
            elif not tour_player :
                x_player_2 = Villes[i][2]
                y_player_2 = Villes[i][3]
                region_player_2 = Villes[i][1]
                tour_player = True
            for i in range(len(Villes)):
                x_ville=Villes[i][2]
                y_ville=Villes[i][3]
                Canevas.create_oval(x_ville+10,y_ville+10,x_ville-10,y_ville-10,fill='yellow')
            L_adj = []
            Canevas.create_oval(x_player_1+10,y_player_1+10,x_player_1-10,y_player_1-10,fill='green')
            Canevas.create_oval(x_player_2+10,y_player_2+10,x_player_2-10,y_player_2-10,fill='orange')
            if not tour_player:
                questions(region_player_1)
            elif tour_player:
                questions(region_player_2)

def questions(region):
    '''
    qui affiche la questions en fonction de la region ou l'on se trouve, réponse au choix entre deux posibilités
    '''
    global fenetre
    for i in range(len(question)):
        if region == question[i][0]:
            num_question = randint(0, 3)
            place_reponse = randint(1, 2)
            fenetre = Tk()
            fenetre.title("La Questions")
            Canevas = Canvas(fenetre,width=500,height=250,bg ='white')
            Canevas.pack()
            police = Font(family='Comics sans MS', size=25)
            Canevas.create_text(250,32,text= question[i + num_question][1],fill="black",font=police)
            if place_reponse == 1:
                Lab = Label(fenetre, text = "1- " + question[i + num_question][2], fg ='black', bg ='white')
                Lab.place(x=100,y=100)
                Lab = Label(fenetre, text = "2- " + question[i + num_question][3], fg ='black', bg ='white')
                Lab.place(x=100,y=150)
    
                bouton1=Button(fenetre, text="reponse 1", command= score)
                bouton1.place(x=125,y=200)
    
                bouton=Button(fenetre, text="reponse 2", command= fenetre.destroy)
                bouton.place(x=250,y=200)
            else:
                Lab = Label(fenetre, text = "1- " + question[i + num_question][3], fg ='black', bg ='white')
                Lab.place(x=100,y=100)
                Lab = Label(fenetre, text = "2- " + question[i + num_question][2], fg ='black', bg ='white')
                Lab.place(x=100,y=150)
    
                bouton1=Button(fenetre, text="reponse 1", command= fenetre.destroy)
                bouton1.place(x=125,y=200)
    
                bouton=Button(fenetre, text="reponse 2", command= score)
                bouton.place(x=250,y=200)
            Canevas.bind('<Button-1>',clic)
            fenetre.mainloop()
            break

def score():
    '''
    qui affiche le score de chaque jour et qui augmente de 100 a chaque bonne reponse
    '''
    global fenetre, score_player_1, score_player_2, str_score_player_1, str_score_player_2
    fenetre.destroy()
    if not tour_player :
        score_player_1 += 100
        str_score_player_1.set(str(score_player_1))
    elif tour_player :
        score_player_2 += 100
        str_score_player_2.set(str(score_player_2))
    if score_player_1 == 1000:
        gagner(nom_player_1)
    elif score_player_2 == 1000:
        gagner(nom_player_2)

def gagner(nom_player):
    '''
    qui affiche le gagnant
    '''
    global Fenetre
    Fenetre = Tk()
    Fenetre.title("Gagner")
    Canevas = Canvas(Fenetre,width=300,height=100,bg ='white')
    Canevas.pack()
    police = Font(family='Comics sans MS', size=25)
    Canevas.create_text(150,32,text= "Bravo "+ nom_player + " vous avez gagné",fill="black",font=police)

    bouton1=Button(Fenetre, text="Fermer", command= fermer)
    bouton1.place(x=125,y=75)

    Canevas.bind('<Button-1>',clic)
    Fenetre.mainloop()

def fermer():
    '''
    ferme toutes les fenetres
    '''
    global Fenetre, Mafenetre
    Fenetre.destroy()
    Mafenetre.destroy()

def adjacence():
    '''
    qui trace un trait entre chaque ville adjacente
    '''
    for i in range (len(Adjacence)):
        for j in range (len(Adjacence[i])):
            k=Adjacence[i][j]
            Canevas.create_line(Villes[i][2],Villes[i][3],Villes[k][2],Villes[k][3],fill='black')

##########################################################
##########    Variables ##################################
##########################################################
Region=["Alsace","Aquitaine","Auvergne","Basse_Normandie","Bourgogne","Bretagne","Centre","Champagne_Ardenne","Corse","Franche_Comte","Haute_Normandie","Ile_De_France","Languedoc_Roussillon","Limousin","Lorraine","Midi_Pyrenees","Nord_Pas_De_Calais","Pays_De_La_Loire","Picardie","Poitout_Charntes","Provence_Alpes_Cote_D_Azur","Rhone_Alpes"]
Villes=[["Strasbourg",1,1074,318],["Colmar",1,1053,370],["Bordeaux",2,534,681],["Pau",2,544,851],["Moulins",3,795,529],["Clermont-Ferrand",3,784,609],["Caen",4,552,268],["Alençon",4,585,341],["Nevers",5,790,482],["Dijon",5,905,448],["Rennes",6,471,361],["Vannes",6,395,410],["Bourges",7,729,481],["Tours",7,626,455],["Troyes",8,846,359],["Chaumont",8,913,380],["Bastia",9,1232,879],["Ajaccio",9,1191,953],["Vesoul",10,978,423],["Besancon",10,981,460],["Rouen",11,654,246],["Evreux",11,653,293],["Paris",12,730,317],["Melun",12,754,335],["Perpignan",13,767,896],["Nimes",13,871,789],["Gueret",14,703,560],["Limoges",14,652,593],["Metz",15,973,278],["Epinal",15,998,365],["Foix",16,677,871],["Montauban",16,660,771],["Arras",17,754,169],["Lille",17,775,137],["Angers",18,548,439],["Nantes",18,471,459],["Amiens",19,733,208],["Laon",19,805,242],["La_Rochelle",20,498,563],["Poitiers",20,602,520],["Marseille",21,950,841],["Nice",21,1074,790],["Lyon",22,896,621],["Grenoble",22,964,640 ]]

question=[[1,"Que signifie Strasbourg","la ville aux châteaux","la ville des routes"],[1,"Le musée remarquable du chemin de fer se trouve à ?","Mulhouse","Strasbourg"],[1,"Où se tient chaque année la foire régionale des vins d'Alsace ?","Colmar","Riquewihr"],[1,"Le Rhin prend sa source :","en Suisse","en Allemagne"],
          [2,"Les vignobles du Pomerol et du St Emilion sont des vins :","rouges","blancs"],[2,"Que sont 'les cailloux du gave' à Pau ?","des bonbons","des galets"],[2,"Quel est le plus grand département de France ?","la Gironde","les Landes"],[2,"Quel est le département correspondant au n°40 ?","Landes","Dordogne"],
          [3,"Quel est le légume sec réputé du Puy","la lentille verte","le haricot rouge"],[3,"La Fourme d'Ambert est","un fromage","un gâteau"],[3,"Quel est le plus haut sommet ?","Puy de Sancy","Plomb du Cantal"],[3,"St Pourçain sur Sioule est réputé par :","ses vins","ses sources"],
          [4,"Quelle est le spécalité de Vire","l'andouille","la crêpe"],[4,"Les habitants de la manche s'appellent ?","les Manchois","les Manchots"],[4,"Comment appelle t-on la plage de deauville ?","les planches","les sables"],[4,"Isigny, c'est le beurre,mais quoi encore ?","les caramels","le chocolat"],
          [5,"A quoi fai penser Magny Cours","un circuit automobile","ses caves à vins"],[5,"Quelle rivière traverse Auxerre ?","l'Yonne","l'Aube"],[5,"Quelle est la spécialité du jambon de Bourgogne ?","jambon persillé","jambon à l'os"],[5,"Quelle est la couleur des fameux boeufs charolais ?","blanc-crème","roux"],
          [6,"Morbian en breton signifie :","mer petite","grande mer"],[6,"Qu'est-ce qu'une bombarde ?","un instrument de musique","une fête"],[6,"Le préfixe Ker est fréquent, que signifie-t-il ?","village","paroisse"],[6,"Le Kouign Amân est :"," un gâteau","un poisson"],
          [7,"Quel est le symbole de Français 1er ?","la salamande","l'hermine"],[7,"L'indre est un affluent de la rive droite de la Loire ?","Faux","Vrai"],[7,"Quelle est la ville du vinaigre ?","Orléans","Tours"],[7,"Quelle est la préfecture du Cher ?","Bourges","Tours"],
          [8,"Qu'était le circuit de Gueux,près de Reims ?","automobile","cycliste"],[8,"Quelle est la spécialité culinaire de Ste Menehould","le pied de cochon","les tripes"],[8,"Verdun est en :","Meuse","Haute Marne"],[8,"Quelle est la spécialité industrielle à Troyes ?","la bonneterie","la dentellerie"],
          [9,"Combien y a t-il de département en Corse ?","2","3"],[9,"Quelle maladie fut longtemps le fléau de l'île ?"," le paludisme","la peste"],[9,"Figari est-ce le nom ?","d'un aéroport","d'une figue"],[9,"'vendetta' signifie :","vengeance","bienvenue"],
          [10,"Que fabriquent essentiellement les fruitières du jura ?"," le comté","le camembert"],[10,"Où est né Pasteur ?","Dole","Arbois"],[10,"Quelle est la spécialité de Morez ?","la lunetterie","l'horlogerie"],[10,"Comment appelle t-on une forêt d'épicéas ?","une pessière","une épicière"],
          [11,"Quelle est la ville surnommée 'la ville aux cent clochers' ?","Rouen","le Tréport"],[11,"Qu'appelle t-on des 'rouenneries'?","des toiles de coton","des pâtisseries"],[11,"Quelle est la boisson typiquement normande ?","le cidre","la liqueur"],[11,"Au pied des falaises, les plages en sont riches :","galets","sable"],
          [12,"L'Arc de Triomphe a été construit pour y mettre l soldat inconnu ?","Faux","Vrai"],[12,"Quel est le surnom du Centre Pompidou ?","la raffinerie","la plomberie"],[12,"Quel est le plus vieux pont de Paris ?","le Pont Neuf","le Pont Royal"],[12,"Quel est l'importante production du Val de Marne ?","les orchidées","le mimosa"],
          [13,"Comment appelle t-on ce vent violent du sud et du sud-est ?"," le vent d'antan","le vent d'hier"],[13,"La coueleur des chevaux de Camargue est noire","Faux","Vrai"],[13,"Castelnaudray est réputée pour :","son cassoulet","ses fromages"],[13,"Que trouve t-on dans le massif du Canigou ?","du fer","de la nourriture pour chien"],
          [14,"Quelle est la capitale de la région limousine ?","Limoges","Tulle"],[14,"Quel est l'arbre typique du limousin ?"," le châtaignier","l'olivier"],[14,"Qu'est ce qu'un moutier ?","un monastère","une horloge"],[14,"Guéret est le chef lieu de quel département ?","la Creuse","la Haute Vienne"],
          [15,"Quel est le bonbon fabriqué à Nancy ?"," la bergamote","les calissons"],[15,"La 'Minette' désigne le minerai de fer lorrain ,","Vrai","Faux"],[15,"Quel fruit de Meurthe et Moselle est connu ?","la mirabelle","la pomme"],[15,"Quelle est la psécialité de Commercy ?","les madeleines","les gaufres"],
          [16,"Quelle langue parle t-on dans la région de Foix ?","la langue d'oc","la lagnue d'oil"],[16,"Comment surnomme t-on la ville Toulouse ?","la ville rose","la ville blanche"],[16,"Qu'est ce que l'autan ?","un vent chaud","un vent froid"],[16,"Quelle est cette célèbre eau de vie, originaire du Gers ?","l'Armagnac","le Cognac"],
          [17,"Où se trouve la colonne de la Grande Armée ?","Berck-Plage","Boulogne sur mer"],[17,"Quelle ville est célèbre par son clair de lune ?","Maubeuge","Dunkerque"],[17,"Quelle est l cité de la laine ?","Roubaix","Tourcoing"],[17,"Le département qui correspond au 62 est le Pas de Calais ?","Vrai","Faux"],
          [18,"Quelle est la rivière qui traverse Angers","la Maine","la Sarthe"],[18,"Qu'est-ce que la civelle?","la larve d'anguille","un vin de pays"],[18,"Quel est l'endroit célèbre pour ses marais salants ?","Guérande","le Pouliguen"],[18,"Quelle est la préfecture de Maine et Loire ?","Angers","La  Roch sur Yon"],
          [19,"Où l'Oise prend-elle sa source ?","en Belgique","en Allemagne"],[19,"Le 'Rollot' spécialité de la région est :","un fromage","un gâteau rond"],[19,"Quelle activité a rendu la région d'Amiens prospère au Moyen Age ?","la teinturerie","l'orfèvrerie"],[19,"Où se trouve la plus importante usine Nestlé de France ?","à Boué","à Braine"],
          [20,"Comment est appelée l'île de Ré ?","l'île blanche","l'île rose"],[20,"Quelle est la friandise réputée de Poitiers ?","la nougatine","la nougat"],[20,"A Royan qu'appelle t-on 'conche' ?","une plage","un bateau"],[20,"Comment appelle t-on la culture des moules ?","la mytiliculture","la mouciliculture"],
          [21,"Que récolte t-on en Camargue ?","le riz","le soja"],[21,"Quelle est la fleur qui image la Côte d'Azur ?","le mimosa","le lilas"],[21,"Quel est le pays des melons ?","Cavaillon","Castellane"],[21,"Quelle ville est appelée 'la cité des papes' ?","Avigon","Grasse"],
          [22,"Le pont de l'Arc de l'Ardèche est :","une arche calcaire naturelle","un viaduc"],[22,"Quel est le grand parc de Lyon ?","le parc de la tête d'or","le parc de la tête d'argent"],[22,"Oyonnax est la capital :","du plastique","du fer"],[22,"Quelle est la spécialité de Montélimar ?","le nougat","les crêpes"]]



Adjacence=[[1,28],[0,29],[38,27,3],[2,31,30],[8,5],[4,42,31],[10,7,20],[6,21,13],[12,4,9],[15,19,18,42,8],[11,34,6],[10,35],[23,8,26],[23,7,34,39],[23,37,15],[14,28,9],[41,17],[16,41],[9,19,29],[18,9],[6,36,21],[20,7,22],[21,37,23],[23,22,14,13,12],[30,25],[24,40,43],[12,39,27],[2,26],[37,15,29,0],[28,1,18],[3,24,31],[3,30,5,2],[36,33],[32,37],[10,35,13],[11,34,38],[32,20],[22,14,28,33],[35,39,2],[13,26,38],[25,41],[40,16,17],[5,9,43],[42,25,41]] # la ville 0 touche les villes 2,4,5

x_player_1 = 534
y_player_1 = 681
nom_player_1 = ''
couleur_player_1 = 'green'
region_player_1 = 2
score_player_1 = 0

x_player_2 = 1074
y_player_2 = 318
nom_player_2 =''
couleur_player_2 = 'orange'
region_player_2 = 1
score_player_2 = 0

tour_player = True

x_clic = 0
y_clic = 0

L_adj = []

#########################################################
########## Interface graphique ##########################
##########################################################
Mafenetre = Tk()
Mafenetre.title("La France en 2000 Questions")
Canevas = Canvas(Mafenetre,width=1500,height=1000,bg ='white')
Canevas.pack()
police = Font(family='Comics sans MS', size=25) # création de polices
Mapolice = Font(family='Comics sans MS', size=45)
Canevas.create_text(728,32,text="La France en quelques questions ",fill="black",font=Mapolice)
Canevas.create_line(282,69,1158,67,fill='black')

bouton1=Button(Mafenetre, text="Lancer le dé", command=de)
bouton1.place(x=269,y=947)

bouton=Button(Mafenetre, text="Valider", command=aff)
bouton.place(x=43,y=250)
Lab = Label(Mafenetre, text = "joueurs", fg ='red', bg ='white')
Lab.place(x=43,y=206)
Champ = Entry(Mafenetre, bg ='bisque', fg='maroon')
Champ.focus_set()
Champ.place(x=43,y=225)

bouton2=Button(Mafenetre, text="GO", command=fin)
bouton2.place(x=177,y=319)

bouton3=Button(Mafenetre, text="afficher les joueurs", command=prenom)
bouton3.place(x=43,y=284)

fichier=PhotoImage(file='carte.gif')
img=Canevas.create_image(750,550,image=fichier)

for i in range(len(Villes)):
    x=Villes[i][2]
    y=Villes[i][3]
    Canevas.create_oval(x+10,y+10,x-10,y-10,fill='yellow') # création des ronds sur chaque ville

str_score_player_1 = StringVar()
str_score_player_1.set(str(score_player_1))

str_score_player_2 = StringVar()
str_score_player_2.set(str(score_player_2))

###########################################################
########### Receptionnaire d'évènement ####################
###########################################################
Canevas.bind('<Button-1>',clic)

##########################################################
############# Programme principal ########################
##########################################################

###################### FIN ###############################
Mafenetre.mainloop()